# API para criação de Vendas

## Como instalar

Antes de iniciar o desenvolvimento da aplicação, é necessário instalar as suas dependências.

```shell
python3 -m venv env # cria ambiente virtual
source env/bin/active  # ativa o ambiente virtual
pip install -r requirements.txt # instala os requisitos
```

## Configuração

Antes de iniciar o projeto é preciso criar uma base postgres com o nome "nome_da_sua_base" e em seguida configurar as variáveis de ambiente necessárias para um bom funcionamento. Crie uma cópia do arquivo `.env.sample` com o nome de `.env` e altere os valores de suas variáveis internas.

# Executando

Execute o comando abaixo para rodar as migrações no banco de dados:

```shell
python manage.py migrate
```

Execute o comando abaixo e um servidor web poderá ser acessado pelo endereço `http://127.0.0.1:8000/`.

```shell
python manage.py runserver
```

# Requisições

Para efetuar um registro de item, por exemplo, utilize como exemplo o modelo abaixo:

```shell
curl -X POST -H "Content-Type: application/json" \
 -d '{"description": "Nome do item","value":17.00,"comission":0.5,"type":1}' \
 http://localhost:8000/api/items/
 ```

# Testes

Para executar os testes, utilize o comando:

```shell
python manage.py test enterprise.apps.api
```