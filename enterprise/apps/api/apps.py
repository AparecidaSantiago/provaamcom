from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'enterprise.apps.api'
