# Generated by Django 2.2.5 on 2019-11-18 05:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_auto_20191116_2001'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sale',
            name='number',
        ),
    ]
