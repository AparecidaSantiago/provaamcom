from django.db import models
from datetime import datetime, time
from django.utils import timezone

class Item(models.Model):
    """Model for gathering data from a sales item"""
    items_types = (
        (1, "Produto"),
        (2, "Serviço"),
    )
    description = models.CharField(max_length=200, null=True, blank=True)
    value = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    comission = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    type = models.IntegerField(choices=items_types, default=1,null=True, blank=True)

    def __str__(self):
        return "{}-{}".format(self.description, self.value)

class Seller(models.Model):
    """Model for gathering data from a seller"""
    name = models.CharField(max_length=100, null=True, blank=True)
    cpf = models.CharField(max_length=14,null=True, blank=True, unique=True)
    mobile_number = models.CharField(max_length=14, null=True, blank=True)

    def __str__(self):
        return "{}-{}".format(self.name, self.cpf)


class Sale(models.Model):
    """Model for gathering data from a sale"""
    seller = models.ForeignKey(Seller, null=True, blank=True, on_delete=models.CASCADE)
    date_hour_sale = models.DateTimeField(null=True, blank=True)
    total_value_sale = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    total_value_comission = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)

    def __str__(self):
        return "{}/{}/{}".format(self.date_hour_sale, self.seller, self.total_value_sale)

    def save(self, *args, **kwargs):
        """Sobrescreve o comportamento padrão para novas vendas."""

        self.date_hour_sale = datetime.now() # + timedelta(hours=21) - timedelta(minutes=7)
        begin_time = time(00, 00)
        end_time = time(12, 00)
        check_time = self.date_hour_sale.time()
        self.total_value_sale = 0 # Valor da soma dos itens da venda
        self.total_value_comission = 0 # Valor total da comissão sobre a venda

        if 'sale_items' in kwargs:
            sale_items = kwargs['sale_items']

            for sale_item in sale_items:
                item = Item.objects.get(pk=sale_item.item_id)
                self.total_value_sale += item.value

                sale_item.comission = 0
                sale_item.value = item.value

                if self.seller:
                    sale_item.comission = item.comission

                    if check_time >= begin_time and check_time <= end_time:
                        if item.comission > 0.05:
                            sale_item.comission = 0.05
                    elif item.comission < 0.04:
                        sale_item.comission = 0.04

                    self.total_value_comission += float(item.value) * float(sale_item.comission)

            del kwargs['sale_items']

        return super().save(*args, **kwargs)

class SaleItems(models.Model):
    """Model for gathering data from a sale with items"""
    item = models.ForeignKey(Item, null=True, blank=True, on_delete=models.CASCADE)
    sale = models.ForeignKey(Sale, null=True, blank=True, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    comission = models.DecimalField(max_digits=2, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return "{}-{}".format(self.item.description, self.value)