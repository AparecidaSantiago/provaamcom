from rest_framework import serializers
from enterprise.apps.api.models import Item, Seller, Sale, SaleItems

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('description', 'value', 'comission', 'type')
        extra_kwargs = {'description': {'required': True}, 'value': {'required': True},
                        'comission': {'required': True}, 'type': {'required': True}}

class SellerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seller
        fields = ('name', 'cpf', 'mobile_number')
        extra_kwargs = {'cpf': {'required' : True}}

class SaleItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SaleItems
        fields = ('value', 'comission', 'description')

    description = serializers.SerializerMethodField('get_description')

    def get_description(self, obj):
        return obj.item.description
    
class SaleSerializer(serializers.ModelSerializer):
    items = SaleItemsSerializer(many=True)
    class Meta:
        model = Sale
        fields = '__all__'
        extra_kwargs = {'seller': {'required': True}}
        depth = 1

class SalePostSerializer(serializers.ModelSerializer):
    items = serializers.ListField(required=True)

    class Meta:
        model = Sale
        fields = '__all__'

    def create(self):
        del self.validated_data['items']
        return Sale(**self.validated_data)

class SalePutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sale
        fields = '__all__'
