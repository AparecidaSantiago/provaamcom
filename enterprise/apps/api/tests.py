import json

from rest_framework import status
from django.test import TestCase, Client
from enterprise.apps.api.serializers import *

client = Client()

"""Start tests fot Sellers"""
class GetAllSellersTest(TestCase):
    """ Test module for GET all sellers API """

    def setUp(self):
        Seller.objects.create(
            name='Teste Seller One', cpf="331.050.820-00", mobile_number="85976541234")
        Seller.objects.create(
            name='Teste Seller Two', cpf="916.150.610-96", mobile_number="8599875412")
        Seller.objects.create(
            name='Teste Seller Three', cpf="234.758.660-21", mobile_number="83987651234")

    def test_get_all_sellers(self):
        # get API response
        response = self.client.get('http://localhost:8000/api/sellers')
        sellers = Seller.objects.all()
        serializer = SellerSerializer(sellers, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class GetSingleSellerTest(TestCase):
    """ Test module for GET single seller API """

    def setUp(self):
        self.seller1 = Seller.objects.create(
            name='Teste Seller Four', cpf="319.021.450-62", mobile_number="85976541234")
        self.seller2 = Seller.objects.create(
            name='Teste Seller Five', cpf="916.150.610-96", mobile_number="8599875412")
        self.seller3 = Seller.objects.create(
            name='Teste Seller Six', cpf="234.758.660-21", mobile_number="83987651234")
    def test_get_valid_single_seller(self):
        response = self.client.get('http://localhost:8000/api/seller/%s' %(self.seller1.pk))

        seller = Seller.objects.get(pk=self.seller1.pk)
        serializer = SellerSerializer(seller)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_seller(self):
        response = self.client.get('http://localhost:8000/api/seller', kwargs={'pk': 100})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewSellerTest(TestCase):
    """ Test module for inserting a new seller """

    def setUp(self):
        self.valid_payload = {
            'name': 'New Seller One',
            'cpf': '768.564.450-91',
            'mobile_number': '86986546700'
        }
        self.invalid_payload = {
            'name': 'New Seller One',
            'mobile_number': ''
        }

    def test_create_valid_seller(self):
        response = self.client.post('http://localhost:8000/api/sellers',
                                    data=json.dumps(self.valid_payload),
                                    content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_seller(self):
        response = self.client.post('http://localhost:8000/api/sellers',
                                    data=json.dumps(self.invalid_payload),
                                    content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleSellerTest(TestCase):
    """ Test module for updating an existing seller record """

    def setUp(self):
        self.seller1 = Seller.objects.create(
            name='Teste Seller Four', cpf="319.021.450-62", mobile_number="85976541234")
        self.seller2 = Seller.objects.create(
            name='Teste Seller Five', cpf="916.150.610-96", mobile_number="8599875412")

        self.valid_payload = {
            'name': 'New Seller Four',
            'cpf': '319.021.450-62',
            'mobile_number': '85976541234'
        }
        self.invalid_payload = {
            'name': 'New Seller Four',
            'mobile_number': '85976541234'
        }

    def test_valid_update_seller(self):
        response = client.put('http://localhost:8000/api/seller/%s' %(self.seller1.pk),
                              data=json.dumps(self.valid_payload),content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_update_seller(self):
        response = client.put('http://localhost:8000/api/seller/%s' %(self.seller1.pk),
                              data=json.dumps(self.invalid_payload),content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleSellerTest(TestCase):
    """ Test module for deleting an existing seller record """

    def setUp(self):
        self.seller1 = Seller.objects.create(
            name='Teste Seller Four', cpf="319.021.450-62", mobile_number="85976541234")
        self.seller2 = Seller.objects.create(
            name='Teste Seller Five', cpf="916.150.610-96", mobile_number="8599875412")

    def test_valid_delete_seller(self):
        response = client.delete('http://localhost:8000/api/seller/%s' % (self.seller1.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_seller(self):
        response = client.delete('http://localhost:8000/api/seller/', kwargs={'pk': 90})

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
"""End of tests for Seller"""


"""Start tests for Items"""
class GetAllItemsTest(TestCase):
    """ Test module for GET all items API """

    def setUp(self):
        Item.objects.create(description='Product One', value="789.00", comission="0.5", type=1)
        Item.objects.create(description='Product Two', value="56.70", comission="0.1", type=1)
        Item.objects.create(description='Service One', value="459.00", comission="0.3", type=2)

    def test_get_all_items(self):
        # get API response
        response = self.client.get('http://localhost:8000/api/items')
        items = Item.objects.all()
        serializer = ItemSerializer(items, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class GetSingleItemTest(TestCase):
    """ Test module for GET single item API """

    def setUp(self):
        self.item1 = Item.objects.create(description='Product One', value="789.00", comission="0.5", type=1)
        self.item2 = Item.objects.create(description='Product Two', value="56.70", comission="0.1", type=1)
        self.item3 = Item.objects.create(description='Service One', value="459.00", comission="0.3", type=2)

    def test_get_valid_single_item(self):
        response = self.client.get('http://localhost:8000/api/item/%s' %(self.item1.pk))

        item = Item.objects.get(pk=self.item1.pk)
        serializer = ItemSerializer(item)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_item(self):
        response = self.client.get('http://localhost:8000/api/item', kwargs={'pk': 100})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewItemTest(TestCase):
    """ Test module for inserting a new item """

    def setUp(self):
        self.valid_payload = {
            'description': 'Product One',
            'value' : "789.00",
            'comission': "0.5",
            'type': 1
        }
        self.invalid_payload = {
            'value' : "789.00",
            'comission': "0.5",
            'type': 1
        }

    def test_create_valid_item(self):
        response = self.client.post('http://localhost:8000/api/items',
                                    data=json.dumps(self.valid_payload),
                                    content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_item(self):
        response = self.client.post('http://localhost:8000/api/items',
                                    data=json.dumps(self.invalid_payload),
                                    content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleItemTest(TestCase):
    """ Test module for updating an existing item record """

    def setUp(self):
        self.item1 = Item.objects.create(description='Product Two', value="56.70", comission="0.1", type=1)
        self.item2 = Item.objects.create(description='Service One', value="459.00", comission="0.3", type=2)

        self.valid_payload = {
            'description': 'Product Four',
            'value': "125.70",
            'comission': "0.0",
            'type': 1
        }
        self.invalid_payload = {
            'description': 'Product Four',
            'value': "125.70",
            'type': 1
        }

    def test_valid_update_item(self):
        response = client.put('http://localhost:8000/api/item/%s' % (self.item1.pk),
                              data=json.dumps(self.valid_payload),content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_update_item(self):
        response = client.put('http://localhost:8000/api/item/%s' % (self.item1.pk),
                              data=json.dumps(self.invalid_payload),content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleItemTest(TestCase):
    """ Test module for deleting an existing item record """

    def setUp(self):
        self.item1 = Item.objects.create(description='Product Five', value="56.70", comission="0.1", type=1)
        self.item2 = Item.objects.create(description='Service Six', value="459.00", comission="0.3", type=2)

    def test_valid_delete_item(self):
        response = client.delete('http://localhost:8000/api/item/%s' % (self.item1.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_item(self):
        response = client.delete('http://localhost:8000/api/item/', kwargs={'pk': 50})

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
"""End tests for Items"""

"""Start tests for Sales"""
class GetAllSalesTest(TestCase):
    """ Test module for GET all sales API """

    def setUp(self):
        Seller.objects.create(name='Teste Seller One', cpf="116.990.950-78", mobile_number="21987452014")

    def test_get_all_sales(self):
        # get API response
        response = self.client.get('http://localhost:8000/api/sales')
        sales = Sale.objects.all()
        serializer = SaleSerializer(sales, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class GetSingleSaleTest(TestCase):
    """ Test module for GET single sale API """

    def setUp(self):
        self.seller = Seller.objects.create(name='Teste Seller One', cpf="853.743.000-59", mobile_number="21987452014")
        self.sale1 = Sale.objects.create(seller=self.seller)


    def test_get_valid_single_sale(self):
        response = self.client.get('http://localhost:8000/api/sale/%s' % (self.sale1.pk))

        sale = Sale.objects.get(pk=self.sale1.pk)
        serializer = SaleSerializer(sale)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_sale(self):
        response = self.client.get('http://localhost:8000/api/sale', kwargs={'pk': 100})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewSaleTest(TestCase):
    """ Test module for inserting a new sale """

    def setUp(self):
        self.seller = Seller.objects.create(name='Teste Seller One', cpf="853.743.000-59", mobile_number="21987452014")

        self.valid_payload = {
            'seller': self.seller.pk
        }
        self.invalid_payload = {

        }

    def test_create_valid_item(self):
        response = self.client.post('http://localhost:8000/api/items',
                                    data=json.dumps(self.valid_payload),
                                    content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_item(self):
        response = self.client.post('http://localhost:8000/api/items',
                                    data=json.dumps(self.invalid_payload),
                                    content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleItemTest(TestCase):
    """ Test module for updating an existing item record """

    def setUp(self):
        self.item1 = Item.objects.create(description='Product Two', value="56.70", comission="0.1", type=1)
        self.item2 = Item.objects.create(description='Service One', value="459.00", comission="0.3", type=2)

        self.valid_payload = {
            'description': 'Product Four',
            'value': "125.70",
            'comission': "0.0",
            'type': 1
        }
        self.invalid_payload = {
            'description': 'Product Four',
            'value': "125.70",
            'type': 1
        }

    def test_valid_update_item(self):
        response = client.put('http://localhost:8000/api/item/%s' % (self.item1.pk),
                              data=json.dumps(self.valid_payload),content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_update_item(self):
        response = client.put('http://localhost:8000/api/item/%s' % (self.item1.pk),
                              data=json.dumps(self.invalid_payload),content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleItemTest(TestCase):
    """ Test module for deleting an existing item record """

    def setUp(self):
        self.item1 = Item.objects.create(description='Product Five', value="56.70", comission="0.1", type=1)
        self.item2 = Item.objects.create(description='Service Six', value="459.00", comission="0.3", type=2)

    def test_valid_delete_item(self):
        response = client.delete('http://localhost:8000/api/item/%s' % (self.item1.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_item(self):
        response = client.delete('http://localhost:8000/api/item/', kwargs={'pk': 50})

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)