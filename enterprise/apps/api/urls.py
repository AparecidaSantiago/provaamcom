from django.urls import path
from . import views
from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(title='Amcom API')

app_name = "api"
urlpatterns = [
    # Default : documentation
    path('', schema_view),
    # Sellers
    path('sellers', views.Sellers.as_view(), name="sellers"),
    path('seller/<pk>', views.Sellers.as_view(), name="seller"),
    # Items
    path('items', views.Items.as_view(), name="items"),
    path('item/<pk>', views.Items.as_view(), name="item"),
    # Sales
    path('sales', views.Sales.as_view(), name="sales"),
    path('sale/<pk>', views.Sales.as_view(), "sale"),
    path('solditems', views.SoldItems.as_view(), name="solditems"),
]
