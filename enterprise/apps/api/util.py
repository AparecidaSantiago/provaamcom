"""
Funções disponíveis para realização de ações corriqueiras de forma simplificada
"""
from datetime import datetime, timedelta
from django.utils import timezone
from django.contrib.admin.models import LogEntry, ADDITION, DELETION, CHANGE
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import force_str


def log_create_object(obj, user, message=None):
    """
    Cria registro de log, ao criar persiste um novo objeto no banco
    """
    LogEntry.objects.create(
        user_id=user.pk,
        content_type_id=ContentType.objects.get_for_model(obj).pk,
        object_id=obj.pk,
        object_repr=force_str(obj),
        action_flag=ADDITION,
        change_message=message,
        action_time=(timezone.now() - timedelta(hours=3)),
    )


def log_edit_object(obj, user, message):
    """
    Cria registro de log para alterações em objetos
    """
    LogEntry.objects.create(
        content_type_id=ContentType.objects.get_for_model(obj).pk,
        object_id=obj.pk,
        object_repr=force_str(obj),
        action_flag=CHANGE,
        change_message=message,
        action_time=(timezone.now() - timedelta(hours=3)),
    )


def log_delete_object(obj, user, message):
    """
    Cria registro de log para deleções de objetos no banco
    """
    LogEntry.objects.create(
        content_type_id=ContentType.objects.get_for_model(obj).pk,
        object_id=obj.pk,
        object_repr=force_str(obj),
        action_flag=DELETION,
        change_message=message,
        action_time=(timezone.now() - timedelta(hours=3)),
    )
