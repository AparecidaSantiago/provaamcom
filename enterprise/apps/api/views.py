from enterprise.apps.api.models import *
from enterprise.apps.api.serializers import *
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class Sellers(APIView):
    """
    List all sellers, or create a new seller.
    """
    def get(self, request, pk=None):
        if pk:
            try:
                seller = Seller.objects.get(pk=pk)
            except:
                raise Http404
        else:
            seller = list(Seller.objects.all())

        serializer = SellerSerializer(seller, many=isinstance(seller, list))
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = SellerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def put(self, request, pk, format=None):
        seller = Seller.objects.get(pk=pk)
        serializer = SellerSerializer(seller, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        seller = Seller.objects.get(pk=pk)
        seller.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class Items(APIView):
    """
    List all items, or create a new item.
    """
    def get(self, request, pk=None):
        if pk:
            try:
                data = Item.objects.get(pk=pk)
            except:
                raise Http404
        else:
            params = request.GET
            data = Item.objects.all()

            if 'type' in params:
                data = data.filter(type=params['type'])

            data = list(data)

        serializer = ItemSerializer(data, many=isinstance(data, list))
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ItemSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def put(self, request, pk, format=None):
        item = Item.objects.get(pk=pk)
        serializer = ItemSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        item = Item.objects.get(pk=pk)
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class Sales(APIView):
    """
    List all sales, or create a new sale.
    """
    def get(self, request, pk=None):
        if pk:
            try:
                sale = Sale.objects.get(pk=pk)
                items = SaleItems.objects.filter(sale_id=sale.pk)
                sale.items = items
            except:
                raise Http404
        else:
            sale = list(Sale.objects.all())
            for i in sale:
                items = SaleItems.objects.filter(sale_id=i.pk)
                i.items = items

        serializer = SaleSerializer(sale, many=isinstance(sale, list))
        return Response(serializer.data)

    def post(self, request):
        serializer = SalePostSerializer(data=request.data)

        if serializer.is_valid():
            sale_items = []
            for item_id in request.data['items']:
                sale_item = SaleItems()
                sale_item.item_id = item_id
                sale_items.append(sale_item)

            sale = serializer.create()
            sale.save(sale_items=sale_items)

            for sale_item in sale_items:
                sale_item.sale_id = sale.id
                sale_item.save()

            sale.items = sale_items
            serializer = SaleSerializer(sale)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        sale = Sale.objects.get(pk=pk)
        serializer = SalePutSerializer(sale, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        sale = Sale.objects.get(pk=pk)
        sale.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class SoldItems(APIView):
    """
    List all sold items, or create a new sold item.
    """
    def get(self, request):
        """Método que lista todas as vendas que contem
        itens do tipo produto ou serviço"""

        params = request.GET
        items = Item.objects.all()
        data = []

        if 'type' in params:
            items = items.filter(type=params['type'])

        for item in items:
            data += list(SaleItems.objects.filter(item_id=item.pk))

        serializer = SaleItemsSerializer(data, many=True)

        return Response(serializer.data)
